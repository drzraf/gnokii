/*

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Copyright (C) 2014   Raphael Droz <raphael.droz+floss@gmail.com>

  This file provides an API for accessing Nokia internal functions via USB.
  See README for more details on supported mobile phones.

*/

#ifndef _gnokii_links_nokusb_h
#define _gnokii_links_nokusb_h

#include "fbus-common.h"

#define PHONET_FRAME_MAX_LENGTH    1010
#define PHONET_TRANSMIT_MAX_LENGTH 1010
#define PHONET_CONTENT_MAX_LENGTH  1000

/* This byte is at the beginning of all GSM Frames sent over PhoNet. */
#define FBUS_PHONET_FRAME_ID 0x14

/* This byte is at the beginning of all GSM Frames sent over Bluetooth to Nokia 6310
   family phones. */
#define FBUS_PHONET_BLUETOOTH_FRAME_ID  0x19

#define FBUS_PHONET_DKU2_FRAME_ID  0x1b
#define FBUS_PHONET_DKU2_DEVICE_PC  0x0c

/* Our PC in the Nokia 6310 family over Bluetooth. */
#define FBUS_PHONET_BLUETOOTH_DEVICE_PC 0x10

gn_error nokusb_initialise(struct gn_statemachine *state);

typedef struct {
	int buffer_count;
	enum fbus_rx_state state;
	int message_source;
	int message_destination;
	int message_type;
	int message_length;
	char *message_buffer;
	int message_corrupted;
} nokusb_incoming_message;

#endif   /* #ifndef _gnokii_links_nokusb_h */
