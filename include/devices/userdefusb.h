/*

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Copyright (C) 2014	   Raphael Droz <raphael.droz+floss@gmail.com>
*/

#ifndef _gnokii_userdefusb_h
#define _gnokii_userdefusb_h

#ifdef HAVE_LIBUSB
#  include <usb.h>
#endif

#include "compat.h"
#include "misc.h"
#include "gnokii.h"

#ifdef HAVE_LIBUSB

/* Information about a USB DKU2 FBUS interface present on the system */
struct usb_interface_transport {
	struct usb_interface_transport *prev, *next;	/* Next and previous interfaces in the list */
	struct usb_device *device;		/* USB device that has the interface */
	int configuration;			/* Device configuration */
	int configuration_description;		/* Configuration string descriptor number */
	int control_interface;			/* DKU2 FBUS master interface */
	int control_setting;			/* DKU2 FBUS master interface setting */
	int control_interface_description;	/* DKU2 FBUS master interface string descriptor number
						 * If non-zero, use usb_get_string_simple() from
						 * libusb to retrieve human-readable description
						 */
	int data_interface;			/* DKU2 FBUS data/slave interface */
	int data_idle_setting;			/* DKU2 FBUS data/slave idle setting */
	int data_interface_idle_description;	/* DKU2 FBUS data/slave interface string descriptor number
						 * in idle setting */
	int data_active_setting;		/* DKU2 FBUS data/slave active setting */
	int data_interface_active_description;	/* DKU2 FBUS data/slave interface string descriptor number
						 * in active setting */
	int data_endpoint_read;			/* DKU2 FBUS data/slave interface read endpoint */
	int data_endpoint_write;		/* DKU2 FBUS data/slave interface write endpoint */
	usb_dev_handle *dev_control;		/* libusb handler for control interace */
	usb_dev_handle *dev_data;		/* libusb handler for data interface */
};

/* USB-specific FBUS interface information */
typedef struct {
	/* Manufacturer, e.g. Nokia */
	char *manufacturer;
	/* Product, e.g. Nokia 6680 */
	char *product;
	/* Product serial number */
	char *serial;
	/* USB device configuration description */
	char *configuration;
	/* Control interface description */
	char *control_interface;
	/* Idle data interface description, typically empty */
	char *data_interface_idle;
	/* Active data interface description, typically empty */
	char *data_interface_active;
	/* Internal information for the transport layer in the library */
	struct usb_interface_transport *interface;
} usb_interface;

/* Nokia is the vendor we are interested in */
#define NOKIA_VENDOR_ID	0x0421

/* Nokia S40 specific altsetting extra value */
#define NOKIA_S40_USB_TYPE		0xfd

#define USB_MAX_STRING_SIZE		256
#define USB_TIMEOUT			100 /* 0.1 seconds */

#endif

int userdefusb_open(struct gn_statemachine *state);
int userdefusb_close(struct gn_statemachine *state);
int userdefusb_write(const __ptr_t bytes, int size, struct gn_statemachine *state);
int userdefusb_read(__ptr_t bytes, int size, struct gn_statemachine *state);
int userdefusb_select(struct timeval *timeout, struct gn_statemachine *state);

#endif /* _gnokii_userdefusb_h */
