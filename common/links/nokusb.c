/*

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.
  Copyright (C) 2014      Raphaël Droz
*/

#include "config.h"

/* System header files */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <usb.h>

/* Various header file */

#include "compat.h"
#include "misc.h"
#include "gnokii.h"
#include "device.h"
#include "links/nokusb.h"
#include "links/utils.h"
#include "gnokii-internal.h"

static gn_error nokusb_send_message(unsigned int messagesize, unsigned char messagetype, unsigned char *message, struct gn_statemachine *state);

/* FIXME - win32 stuff! */

#define FBUS_CONTENT_MAX_LENGTH	0xfff


/*--------------------------------------------*/


static int send_command(char *cmd, int len, struct gn_statemachine *state)
{
	struct timeval timeout;
	unsigned char buffer[255];
	int res, offset = 0, waitformore = 1;

	/* Communication with the phone looks strange here. I am unable to
	 * read the whole answer from the port with DKU-5 cable and
	 * Nokia 6020. The following code seems to work reliably.
	 */
	device_write(cmd, len, state);
	/* Experimental timeout */
	timeout.tv_sec	= 0;
	timeout.tv_usec	= 500000;

	res = device_select(&timeout, state);
	/* Read from the port only when select succeeds */
	while (res > 0 && waitformore) {
		/* Avoid 'device temporarily unavailable' error */
		usleep(50000);
		res = device_read(buffer + offset, sizeof(buffer) - offset, state);
		/* The whole answer is read */
		if (strstr(buffer, "OK"))
			waitformore = 0;
		if (res > 0)
			offset += res;
		res = offset;
		/* The phone is already in AT mode */
		if (strchr(buffer, 0x55))
			res = 0;
	}
	return res;
}

static bool nokusb_open(struct gn_statemachine *state, gn_connection_type type)
{
	if (!state)
		return false;

	/* Open device. */
	if (!device_open(state->config.port_device, false, false, false, type, state)) {
		perror(_("Couldn't open USB device"));
		return false;
	}

	return true;
}


/* This is the main loop function which must be called regularly */
/* timeout can be used to make it 'busy' or not */

static gn_error nokusb_loop(struct timeval *timeout, struct gn_statemachine *state)
{
	unsigned char	buffer[BUFFER_SIZE];
	int		count, res;

	res = device_select(timeout, state);

	if (res > 0) {
		res = device_read(buffer, sizeof(buffer), state);
		// sm_incoming_function(state->message_type, state->message_buffer, state->message_length, state);
		// phonet_rx_statemachine(buffer[count], state);
		
		if (res > 0) {
			return GN_ERR_NONE;	/* This traps errors from device_read */
		}
	} else if (!res) {
		return GN_ERR_TIMEOUT;
	}

	return error;
}



/* Prepares the message header and sends it, prepends the message start byte
	   (0x1e) and other values according the value specified when called.
	   Calculates checksum and then sends the lot down the pipe... */

int nokusb_tx_send_frame(u8 message_length, u8 message_type, u8 *buffer, struct gn_statemachine *state)
{
	u8 out_buffer[0xfff]; // TODO
	int count, current = 0;
	unsigned char checksum;

	/* FIXME - we should check for the message length ... */

	/* Now construct the message header. */

	
	out_buffer[current++] = 'y'; //FBUS_FRAME_ID;		/* Start of the frame indicator */

	out_buffer[current++] = 'x'; // FBUS_DEVICE_PHONE;		/* Destination */
	out_buffer[current++] = 'z'; // FBUS_DEVICE_PC;			/* Source */

	out_buffer[current++] = message_type;			/* Type */

	out_buffer[current++] = 0;				/* Length */

	out_buffer[current++] = message_length;			/* Length */

	/* Copy in data if any. */

	if (message_length != 0) {
		memcpy(out_buffer + current, buffer, message_length);
		current += message_length;
	}

	/* If the message length is odd we should add pad byte 0x00 */
	if (message_length % 2)
		out_buffer[current++] = 0x00;

	/* Now calculate checksums over entire message and append to message. */

	/* Odd bytes */

	checksum = 0;
	for (count = 0; count < current; count += 2)
		checksum ^= out_buffer[count];

	out_buffer[current++] = checksum;

	/* Even bytes */

	checksum = 0;
	for (count = 1; count < current; count += 2)
		checksum ^= out_buffer[count];

	out_buffer[current++] = checksum;

	/* Send it out... */

	if (device_write(out_buffer, current, state) != current)
		return false;

	return true;
}


/* Main function to send an fbus message */
/* Splits up the message into frames if necessary */

static gn_error nokusb_send_message(unsigned int messagesize, unsigned char messagetype, unsigned char *message, struct gn_statemachine *state)
{
	u8 frame_buffer[FBUS_CONTENT_MAX_LENGTH + 2];
	u8 nom, lml;		/* number of messages, last message len */
	int i;

	if (messagesize > FBUS_CONTENT_MAX_LENGTH) {

		nom = (messagesize + FBUS_CONTENT_MAX_LENGTH - 1)
		    / FBUS_CONTENT_MAX_LENGTH;
		lml = messagesize - ((nom - 1) * FBUS_CONTENT_MAX_LENGTH);

		for (i = 0; i < nom - 1; i++) {

			memcpy(frame_buffer, message + (i * FBUS_CONTENT_MAX_LENGTH),
			       FBUS_CONTENT_MAX_LENGTH);
			frame_buffer[FBUS_CONTENT_MAX_LENGTH] = nom - i;
			frame_buffer[FBUS_CONTENT_MAX_LENGTH + 1] = 0; // XXX

			fbus_tx_send_frame(FBUS_CONTENT_MAX_LENGTH + 2,
					  messagetype, frame_buffer, state);

		}

		memcpy(frame_buffer, message + ((nom - 1) * FBUS_CONTENT_MAX_LENGTH), lml);
		frame_buffer[lml] = 0x01;
		frame_buffer[lml + 1] = 0; // XXX
		nokusb_tx_send_frame(lml + 2, messagetype, frame_buffer, state);

	} else {

		memcpy(frame_buffer, message, messagesize);
		frame_buffer[messagesize] = 0x01;
		frame_buffer[messagesize + 1] = 0; // XXX
		nokusb_tx_send_frame(messagesize + 2, messagetype,
				  frame_buffer, state);
	}
	return GN_ERR_NONE;
}


static void nokusb_reset(struct gn_statemachine *state)
{
}

/* Initialise variables and start the link */
/* state is only passed around to allow for multiple state machines (one day...) */
gn_error nokusb_initialise(struct gn_statemachine *state)
{
	bool connection = false;

	if (!state)
		return GN_ERR_FAILED;

	/* Fill in the link functions */
	state->link.loop = &nokusb_loop;
	state->link.send_message = &nokusb_send_message;
	state->link.reset = &nokusb_reset;
	state->link.cleanup = NULL;

	/* Check for a valid init length */
	if (state->config.init_length == 0)
		state->config.init_length = 250;


	switch (state->config.connection_type) {
	case GN_CT_USERDEFUSB:
		break;
	case GN_CT_DKU2LIBUSB:
	case GN_CT_Infrared:
	case GN_CT_Tekram:
	case GN_CT_Serial:
	case GN_CT_TCP:
	case GN_CT_DAU9P:
	case GN_CT_DLR3P:
#ifdef HAVE_BLUETOOTH
	case GN_CT_Bluetooth:
#endif
	default:
		perror(_("nokusb protocol is only supported for usb (dku2libusb) connection type"));
		break;
	}

	connection = nokusb_open(state, GN_CT_USERDEFUSB);
	if (!connection) {
		return GN_ERR_FAILED;
	}

	unsigned char init_seq1[] = { 0x21, 0x22, 0x02, 0x0c };
	unsigned char init_seq2[] = { 0x21, 0x22, 0x03, 0x04 };
	unsigned char init_seq3[] = { 0x21, 0x22, 0x02, 0x04 };

	device_write(init_seq1, 4, state);
	device_write(init_seq2, 4, state);
	device_write(init_seq3, 4, state);
	nokusb_reset(state);

	return GN_ERR_NONE;
}
