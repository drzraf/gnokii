/*

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Copyright (C) 2014	   Raphael Droz <raphael.droz+floss@gmail.com>
*/

#include "config.h"
#include "compat.h"
#include "misc.h"
#include "gnokii.h"
#include "devices/dku2libusb.h" // for USB_DT_CS_INTERFACE and 2 helpers
#include "devices/userdefusb.h"

#ifndef HAVE_LIBUSB
int userdefusb_open(struct gn_statemachine *state)
{
	return -1;
}

int userdefusb_close(struct gn_statemachine *state)
{
	return -1;
}

int userdefusb_write(const __ptr_t bytes, int size, struct gn_statemachine *state)
{
	return -1;
}

int userdefusb_read(__ptr_t bytes, int size, struct gn_statemachine *state)
{
	return -1;
}

int userdefusb_select(struct timeval *timeout, struct gn_statemachine *state)
{
	return -1;
}

#else

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#define	DEVINSTANCE(s) (*((usb_interface **)(&(s)->device.device_instance)))

/*
 * Helper function to usb_find_interfaces
 */
/*
static void find_eps(struct usb_interface_transport *iface,
		     struct usb_interface_descriptor data_iface,
		     int *found_active, int *found_idle)
{
	struct usb_endpoint_descriptor *ep0, *ep1;

	fprintf(stderr, "[find_eps]....\n");
	if (data_iface.bNumEndpoints == 2) {
		ep0 = data_iface.endpoint;
		ep1 = data_iface.endpoint + 1;
		if ((ep0->bEndpointAddress & USB_ENDPOINT_IN) &&
		    ((ep0->bmAttributes & USB_ENDPOINT_TYPE_MASK) == USB_ENDPOINT_TYPE_BULK) &&
		    !(ep1->bEndpointAddress & USB_ENDPOINT_IN) &&
		    ((ep1->bmAttributes & USB_ENDPOINT_TYPE_MASK) == USB_ENDPOINT_TYPE_BULK)) {
			*found_active = 1;
			iface->data_active_setting = data_iface.bAlternateSetting;
			iface->data_interface_active_description = data_iface.iInterface;
			iface->data_endpoint_read = ep0->bEndpointAddress;
			iface->data_endpoint_write = ep1->bEndpointAddress;
		}
		if (!(ep0->bEndpointAddress & USB_ENDPOINT_IN) &&
		    ((ep0->bmAttributes & USB_ENDPOINT_TYPE_MASK) == USB_ENDPOINT_TYPE_BULK) &&
		    (ep1->bEndpointAddress & USB_ENDPOINT_IN) &&
		    ((ep1->bmAttributes & USB_ENDPOINT_TYPE_MASK) == USB_ENDPOINT_TYPE_BULK)) {
			*found_active = 1;
			iface->data_active_setting = data_iface.bAlternateSetting;
			iface->data_interface_active_description = data_iface.iInterface;
			iface->data_endpoint_read = ep1->bEndpointAddress;
			iface->data_endpoint_write = ep0->bEndpointAddress;
		}
	}
	if (data_iface.bNumEndpoints == 0) {
		*found_idle = 1;
		iface->data_idle_setting = data_iface.bAlternateSetting;
		iface->data_interface_idle_description = data_iface.iInterface;
	}
}
*/


/*
 * Helper function to usbusb_find_interfaces
 */
static int find_usb_data_interface(unsigned char *buffer, int buflen,
				    struct usb_config_descriptor config,
				    struct usb_interface_transport *iface)
{
	int i, a;
	int found_active = 0;
	int found_idle = 0;

	if (!buffer) {
		dprintf("Weird descriptor references\n");
		return -EINVAL;
	}

	if (buffer [1] != USB_DT_CS_INTERFACE) {
		dprintf("didn't found the 0x24 marker in interface's extras\n");
		return -ENODEV;
	}
	if (buffer [2] != NOKIA_S40_USB_TYPE) {
		dprintf("didn't found 0xfd marker in interface's extras\n");
		return -ENODEV;
	}

	/* Found the slave interface, now find active/idle settings and endpoints */
	iface->data_interface = 0x03;
	/* Loop through all of the interfaces */
	for (i = 0; i < config.bNumInterfaces; i++) {
		/* Loop through all of the alternate settings */
		for (a = 0; a < config.interface[i].num_altsetting; a++) {
			// grabs the endpoint of the (valid) interface we found above
			// (from user defined USB settings)
			if (config.interface[i].altsetting[a].bInterfaceNumber == iface->data_interface) {
				// helper from common/devices/dku2libusb.c
				find_eps((struct fbus_usb_interface_transport *)iface,
					 config.interface[i].altsetting[a],
					 &found_active,
					 &found_idle);
			}
		}
	}

	if (!found_idle) {
		dprintf("No idle setting\n");
		return -ENODEV;
	}
	if (!found_active) {
		dprintf("No active setting\n");
		return -ENODEV;
	}
	dprintf("Found a valid USB interface (read enpoint: %02x, write endpoint: %02x)\n", iface->data_endpoint_read, iface->data_endpoint_write);
	return 0;
}

/*
 * Helper function to usb_find_interfaces
 */
/*
static int get_iface_string(struct usb_dev_handle *usb_handle, char **string, int id)
{
	if (id) {
		if ((*string = malloc(USB_MAX_STRING_SIZE)) == NULL)
			return -ENOMEM;
		*string[0] = '\0';
		return usb_get_string_simple(usb_handle, id, *string, USB_MAX_STRING_SIZE);
	}
	return -EINVAL;
}
*/


/*
 * Helper function to usb_find_interfaces
 */
static struct usb_interface_transport *check_iface(struct usb_device *dev, int c, int i, int a,
						   struct usb_interface_transport *current)
{
	struct usb_interface_transport *next = NULL;

	fprintf(stderr, "[check_iface] PASSED!\n");

	int err;
	unsigned char *buffer = dev->config[c].interface[i].altsetting[a].extra;
	int buflen = dev->config[c].interface[i].altsetting[a].extralen;

	next = malloc(sizeof(struct usb_interface_transport));
	if (next == NULL)
		return current;
	next->device = dev;
	next->configuration = dev->config[c].bConfigurationValue;
	next->configuration_description = dev->config[c].iConfiguration;
	next->control_interface = dev->config[c].interface[i].altsetting[a].bInterfaceNumber;
	next->control_interface_description = dev->config[c].interface[i].altsetting[a].iInterface;
	next->control_setting = dev->config[c].interface[i].altsetting[a].bAlternateSetting;

	err = find_usb_data_interface(buffer, buflen, dev->config[c], next);
	if (err)
		free(next);
	else {
		if (current)
			current->next = next;
		next->prev = current;
		next->next = NULL;
		current = next;
	}
	return current;
}

/*
 * Function usb_find_interfaces ()
 */
static int usb_find_interfaces(struct gn_statemachine *state)
{
	struct usb_bus *busses;
	struct usb_bus *bus;
	struct usb_device *dev;
	int c, i, a, retval = 0;
	struct usb_interface_transport *current = NULL;
	struct usb_interface_transport *tmp = NULL;
	struct usb_dev_handle *usb_handle;
	int n;
	int spec_bConfigurationValue, spec_bInterfaceNumber, spec_bAlternateSetting;
	int user_defined_usbdev = 0;

	/* For connection type dku2libusb port denotes number of DKU2 device */
	n = atoi(state->config.port_device);
	/* Assume default is first interface */
	if (n < 1) {
		n = 1;
		dprintf("port = %s is not valid for connection = dku2libusb using port = %d instead\n", state->config.port_device, n);
	}

	// For connection type dku2libusb bInterfaceNumber can be forced
	spec_bConfigurationValue = state->config.bConfigurationValue;
	spec_bInterfaceNumber = state->config.bInterfaceNumber;
	spec_bAlternateSetting = state->config.bAlternateSetting;

	// if these 3 settings are set assume the user knows what he's doing and
	// ignore the USB_CDC_CLASS and USB_CDC_FBUS_SUBCLASS device's checks
	if(spec_bConfigurationValue >= 0 && spec_bInterfaceNumber >= 0 && spec_bAlternateSetting)
		user_defined_usbdev = 1;

	fprintf(stderr, "bAlternateSetting:%d, bNumInterfaces:%d, bAlternateSetting:%d\n",
		spec_bConfigurationValue, spec_bInterfaceNumber, spec_bAlternateSetting);

	usb_init();
	usb_find_busses();
	usb_find_devices();

	busses = usb_get_busses();

	for (bus = busses; bus; bus = bus->next) {
		for (dev = bus->devices; dev; dev = dev->next) {
			if (dev->descriptor.idVendor == NOKIA_VENDOR_ID) {
				/* Loop through all of the configurations */
				for (c = 0; c < dev->descriptor.bNumConfigurations; c++) {
					if (spec_bConfigurationValue >= 0 && c != spec_bConfigurationValue)
						continue;

					/* Loop through all of the interfaces */
					for (i = 0; i < dev->config[c].bNumInterfaces; i++) {
						if (spec_bInterfaceNumber >= 0 && i != spec_bInterfaceNumber)
							continue;

						/* Loop through all of the alternate settings */
						for (a = 0; a < dev->config[c].interface[i].num_altsetting; a++) {
							if (spec_bAlternateSetting >= 0 && a != spec_bAlternateSetting)
								continue;
					
							/* find data interface */
							fprintf(stderr,
								"[usb_find_interfaces] nokia found, probing bNumConfigurations = %d, bNumInterfaces = %d, num_altsetting = %d\n",
								c, i, a);
							
							current = check_iface(dev, c, i, a, current);
						}
					}
				}
			}
		}
	}

	/* rewind */
	while (current && current->prev)
		current = current->prev;

	/* Take N-th device on the list */
	while (--n && current) {
		tmp = current;
		current = current->next;
		/* free the previous element on the list -- won't be needed anymore */
		free(tmp);
	}

	if (current) {
		int s = sizeof(usb_interface);
		state->device.device_instance = calloc(1, s);
		if (!DEVINSTANCE(state))
			goto cleanup_list;

		DEVINSTANCE(state)->interface = current;
		usb_handle = usb_open(current->device);
		if (usb_handle == NULL)
			goto cleanup_list;
		get_iface_string(usb_handle, &DEVINSTANCE(state)->manufacturer,
			current->device->descriptor.iManufacturer);
		get_iface_string(usb_handle, &DEVINSTANCE(state)->product,
			current->device->descriptor.iProduct);
		get_iface_string(usb_handle, &DEVINSTANCE(state)->serial,
			current->device->descriptor.iSerialNumber);
		get_iface_string(usb_handle, &DEVINSTANCE(state)->configuration,
			current->configuration_description);
		get_iface_string(usb_handle, &DEVINSTANCE(state)->control_interface,
			current->control_interface_description);
		get_iface_string(usb_handle, &DEVINSTANCE(state)->data_interface_idle,
			current->data_interface_idle_description);
		get_iface_string(usb_handle, &DEVINSTANCE(state)->data_interface_active,
			current->data_interface_active_description);
		usb_close(usb_handle);
		retval = 1;
		current = current->next;
	}

cleanup_list:
	while (current) {
		tmp = current->next;
		free(current);
		current = tmp;
	}
	return retval;
}

/*
 * Function usb_free_interfaces ()
 *
 *    Free the list of discovered USB interfaces on the system
 */
static void usb_free_interfaces(usb_interface *iface)
{
	if (iface == NULL)
		return;
	free(iface->manufacturer);
	free(iface->product);
	free(iface->serial);
	free(iface->configuration);
	free(iface->control_interface);
	free(iface->data_interface_idle);
	free(iface->data_interface_active);
	free(iface->interface);
	free(iface);
}

/*
 * Function usb_connect_request (self)
 *
 *    Open the USB connection
 *
 */
static int usb_connect_request(struct gn_statemachine *state)
{
	int ret;

	DEVINSTANCE(state)->interface->dev_data = usb_open(DEVINSTANCE(state)->interface->device);

#ifdef __linux__
	/* Ask to remove any driver bound to this interface (-ENODATA means no driver was bound) */
	ret = usb_detach_kernel_driver_np(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->control_interface);
	if (ret < 0 && ret != -ENODATA) {
		dprintf("Can't detach kernel driver: %d\n", ret);
		goto err1;
	}
#endif

	ret = usb_set_configuration(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->configuration);
	if (ret < 0) {
		dprintf("Can't set configuration: %d\n", ret);
	}

	ret = usb_claim_interface(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->control_interface);
	if (ret < 0) {
		dprintf("Can't claim control interface: %d\n", ret);
		goto err1;
	}

	ret = usb_set_altinterface(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->control_setting);
	if (ret < 0) {
		dprintf("Can't set control setting: %d\n", ret);
		goto err2;
	}

	ret = usb_claim_interface(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->data_interface);
	if (ret < 0) {
		dprintf("Can't claim data interface: %d\n", ret);
		goto err2;
	}

	ret = usb_set_altinterface(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->data_active_setting);
	if (ret < 0) {
		dprintf("Can't set data active setting: %d\n", ret);
		goto err3;
	}
	return 1;

err3:
	usb_release_interface(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->data_interface);	
err2:
	usb_release_interface(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->control_interface);
err1:
	usb_close(DEVINSTANCE(state)->interface->dev_data);
	return 0;
}

/*
 * Function usb_link_disconnect_request (self)
 *
 *    Shutdown the USB link
 *
 */
static int usb_disconnect_request(struct gn_statemachine *state)
{
	int ret;

	if (state->device.fd < 0)
		return 0;
	ret = usb_set_altinterface(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->data_idle_setting);
	if (ret < 0)
		dprintf("Can't set data idle setting %d\n", ret);
	ret = usb_release_interface(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->data_interface);
	if (ret < 0)
		dprintf("Can't release data interface %d\n", ret);
	ret = usb_release_interface(DEVINSTANCE(state)->interface->dev_data, DEVINSTANCE(state)->interface->control_interface);
	if (ret < 0)
		dprintf("Can't release control interface %d\n", ret);
	ret = usb_close(DEVINSTANCE(state)->interface->dev_data);
	if (ret < 0)
		dprintf("Can't close data interface %d\n", ret);
	return ret;	
}

int userdefusb_open(struct gn_statemachine *state)
{
	int retval;

	retval = usb_find_interfaces(state);
	if (retval)
		retval = usb_connect_request(state);
	return (retval ? retval : -1);
}

int userdefusb_close(struct gn_statemachine *state)
{
	usb_disconnect_request(state);
	usb_free_interfaces(DEVINSTANCE(state));
	state->device.device_instance = NULL;
	return 0;
}

int userdefusb_write(const __ptr_t bytes, int size, struct gn_statemachine *state)
{
	return usb_bulk_write(DEVINSTANCE(state)->interface->dev_data,
		DEVINSTANCE(state)->interface->data_endpoint_write,
		(char *) bytes, size, USB_TIMEOUT);
}

int userdefusb_read(__ptr_t bytes, int size, struct gn_statemachine *state)
{
	return usb_bulk_read(DEVINSTANCE(state)->interface->dev_data,
		DEVINSTANCE(state)->interface->data_endpoint_read,
		(char *) bytes, size, USB_TIMEOUT);
}

int userdefusb_select(struct timeval *timeout, struct gn_statemachine *state)
{
	return 1;
}

#endif
